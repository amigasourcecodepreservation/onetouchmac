# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.5.1] - 2018-05-05
### Added
- README, CHANGELOG
- git versioning

### Changed
- Project structure cleanup
- 1TouchMac -> OneTouchMac
- COPYING -> LICENSE.md
- OneTouchMAC to OneTouchMac

