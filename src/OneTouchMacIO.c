/*
* This file is part of OneTouchMac.
* Copyright (C) 2000 Julien Tiphaine
* 
* OneTouchMac is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* OneTouchMac is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OneTouchMac.  If not, see <http://www.gnu.org/licenses/>.
*
*/
#include "OneTouchMacIO.h"

/*********** function to copy files **************/
BOOL CopyFile (char Source[], char Dest[])
{   // vars //
    FILE *F_Source=NULL,*F_Dest=NULL;
    char *buf=NULL;
    FileInfoBlock *fib=NULL;
    BPTR mylock=0;
    BOOL OK=TRUE;

    // lock file and get file infos //
    mylock=Lock(Source,ACCESS_READ);
    fib=AllocMem(sizeof(struct FileInfoBlock),0);
    if (mylock!=0 && fib!=0)
    {   Examine(mylock,fib);
        UnLock(mylock); // unlock file as we have finish getting infos
        buf=AllocMem(fib->fib_Size,MEMF_CLEAR); // we do a buffer with the file's size
    }
    else OK=FALSE;

    // here we copy the file //
    if ((F_Source=fopen(Source,"r")) && (F_Dest=fopen(Dest,"a")))
    {   fread(buf,fib->fib_Size,1,F_Source);
        fprintf(F_Dest,"%s",buf);

    // freeing ressources //
        fclose(F_Source);
        fclose(F_Dest);
    }
    else OK=FALSE;

    if(buf) FreeMem(buf,fib->fib_Size);
    if(fib) FreeMem(fib, sizeof(struct FileInfoBlock));

    return OK;
}

/**************** Save function ******************/
BOOL Save(char *LStartup, char *MStartup, char *RStartup)
{
    // vars //
    FILE *MonFich=NULL,*cfg=NULL ;
    char sep_haut[]=";******************** Added by OneTouchMac ********************\n";
    char cmd[200]  =" OneTouchMac";
    char sep_bas[] ="\n;********************* End of OneTouchMac *********************\n";
    int I=0;
    prefs head[3];
    BOOL OK=FALSE;
    BOOL rem();

    // we delete file created before in order not to write OneTouchMac commande two times //
    rem();

    // create the new Startup-sequence file in RAM://
    MonFich=fopen("RAM:startup-sequence", "w");
    cfg=fopen("S:OneTouchMac.cfg", "w");

    if (MonFich && cfg)
    {    // write things that are always written //
        fwrite (sep_haut, strlen(sep_haut),1,MonFich);

        // tests wich arguments to put //
        if (strlen(LStartup)>0)
        {   strcat(cmd, " LMB ");
            strcat(cmd, LStartup);
            head[I].Taille = strlen(LStartup);
            strcpy(head[I].BT,"LMB");
            fwrite(&head[I],sizeof(prefs),1,cfg);
            fwrite(LStartup,strlen(LStartup),1,cfg);
            I++;
        }
        if (strlen(MStartup)>0)
        {   strcat(cmd, " MMB ");
            strcat(cmd, MStartup);
            head[I].Taille = strlen(MStartup);
            strcpy(head[I].BT,"MMB");
            fwrite(&head[I],sizeof(prefs),1,cfg);
            fwrite(MStartup,strlen(MStartup),1,cfg);
            I++;
        }
        if (strlen(RStartup)>0)
        {   strcat(cmd, " RMB ");
            strcat(cmd, RStartup);
            head[I].Taille = strlen(RStartup);
            strcpy(head[I].BT,"RMB");
            fwrite(&head[I],sizeof(prefs),1,cfg);
            fwrite(RStartup,strlen(RStartup),1,cfg);
        }

    // close config file //
        fclose(cfg);
        // all argument we want are ready, now we finish writting //
        fwrite(cmd, strlen(cmd),1,MonFich);
        fwrite(sep_bas, strlen(sep_bas),1,MonFich);
        fclose(MonFich);

        if(CopyFile("S:Startup-sequence","Ram:Startup-sequence")) // append Startup-sequence to our's in Ram:
            if(Rename("S:Startup-sequence", "S:Startup-sequence.bak")) // We make a backup os user's startup
                if(CopyFile("Ram:Startup-sequence","S:Startup-sequence"))// we replace user's original startup file
                    OK=TRUE;

    }
    DeleteFile("Ram:Startup-sequence");
    return OK;
}

/************** function Rem **************/
BOOL rem()
{   BOOL OK=FALSE;

    DeleteFile("S:OneTouchMac.cfg");
    Rename("S:Startup-sequence","S:Startup-sequence.OTM");
    // we must find Startup-sequence.bak before deleting S:startup-sequence //
    if(Rename("S:Startup-sequence.bak","S:Startup-sequence"))
    {   DeleteFile("S:Startup-sequence.OTM");
        OK=TRUE;
    }
    else
        Rename("S:Startup-sequence.OTM","S:Startup-sequence");// if no backup found, we restore startup-sequence
    return OK;
}
