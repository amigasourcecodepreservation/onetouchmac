/*
* This file is part of OneTouchMac.
* Copyright (C) 2000 Julien Tiphaine
* 
* OneTouchMac is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* OneTouchMac is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OneTouchMac.  If not, see <http://www.gnu.org/licenses/>.
*
*/
#include "demo.h"
#include "OneTouchMacIO.h"

#define BUTTON_OK 1
#define BUTTON_REM 2
#define BUTTON_Cancel 3
#define ID_ABOUT 4

/*************** declaration of function ******************/
void OneTouchMacPrefs();


/****************** Application's Menu ********************/
struct NewMenu Menu[] =
{
    { NM_TITLE, "Project"  , 0 ,0,0,(APTR)0            },
    { NM_ITEM , "About..." ,"?",0,0,(APTR)ID_ABOUT     },
    { NM_ITEM , NM_BARLABEL, 0 ,0,0,(APTR)0            },
    { NM_ITEM , "Quit"     ,"Q",0,0,(APTR)MUIV_Application_ReturnID_Quit },
    { NM_END  , NULL       , 0 ,0,0,(APTR)0            },
};
/*************** End of Menu declaration *****************/


/* here the two main functions: for Cli and for Workbench */
void main() { OneTouchMacPrefs(); }
void wbmain() { OneTouchMacPrefs(); }


/************** Begin Application ************/
void OneTouchMacPrefs()
{
    // vars //
    APTR app,window,Titre,BT_OK,BT_REM,BT_Cancel,Left_Startup,Middle_Startup,Right_Startup;
    BOOL Running = TRUE;
    ULONG sig;
    char *LStartup=NULL,*MStartup=NULL,*RStartup=NULL,Cfg_Startup[200]="";
    int I=0;
    FILE *Cfg=NULL;
    prefs head[3];
    init();

    // GUI declaration //
     app = ApplicationObject,
        MUIA_Application_Title      , "OneTouchMac_Prefs",
        MUIA_Application_Version    , "$VER: OneTouchMac prefs V2.5",
        MUIA_Application_Copyright  , "�2000, Tiphaine Julien",
        MUIA_Application_Author     , "Tiphaine Julien",
        MUIA_Application_Description, "OneTouchMac Prefs GUI",
        MUIA_Application_Base       , "ONETOUCHMAC_PREFS",
        MUIA_Application_Menustrip   , MUI_MakeObject(MUIO_MenustripNM,Menu,0),


        SubWindow, window = WindowObject,
            MUIA_Window_Title, "OneTouchMAc Prefs",
            MUIA_Window_ID   , "Win1",

            WindowContents, VGroup,

                Child, Titre = TextObject,
                               MUIA_Text_Contents, "\33c\33bChoose your Startup for:",
                               End,

                Child, VSpace(10),

                Child, Label("\33lLeft Mouse Button"),
                Child, Left_Startup = PopaslObject,
                              MUIA_Popstring_String, String(0,256),
                              MUIA_Popstring_Button, PopButton(MUII_PopFile),
                              ASLFR_TitleText, "Please select a Startup file...",
                              End,

                Child, VSpace(8),

                Child, Label("\33lMiddle Mouse Button"),
                Child, Middle_Startup = PopaslObject,
                              MUIA_Popstring_String, String(0,256),
                              MUIA_Popstring_Button, PopButton(MUII_PopFile),
                              ASLFR_TitleText, "Please select a Startup file...",
                              End,

                Child, VSpace(8),

                Child, Label("\33lRight Mouse Button"),
                Child, Right_Startup = PopaslObject,
                              MUIA_Popstring_String, String(0,256),
                              MUIA_Popstring_Button, PopButton(MUII_PopFile),
                              ASLFR_TitleText, "Please select a Startup file...",
                              End,

                Child, VSpace(15),

                Child, HGroup,
                    Child, BT_OK = SimpleButton("OK"),
                    Child, BT_REM = SimpleButton("Remove"),
                    Child, BT_Cancel = SimpleButton("CANCEL"),
                    End,
                End,
            End,

        End;
    
    // close if something went wrong //
    if (!app)
        fail(app,"Failed to create Application.");


    // notifications //
    DoMethod(window,MUIM_Notify,MUIA_Window_CloseRequest,TRUE,app,2,MUIM_Application_ReturnID,MUIV_Application_ReturnID_Quit);
    DoMethod(BT_OK,MUIM_Notify,MUIA_Pressed,FALSE,app,2,MUIM_Application_ReturnID,1);
    DoMethod(BT_REM,MUIM_Notify,MUIA_Pressed,FALSE,app,2,MUIM_Application_ReturnID,2);
    DoMethod(BT_Cancel,MUIM_Notify,MUIA_Pressed,FALSE,app,2,MUIM_Application_ReturnID,3);

   // read the config file if exists //
   if (Cfg=fopen("S:OneTouchMac.cfg","r"))
    {   fread(&head[I],sizeof(prefs),1,Cfg);
        while (!feof(Cfg))
        {   fread(&Cfg_Startup,head[I].Taille,1,Cfg);
            if (strcmp(head[I].BT,"LMB")==0)
                set(Left_Startup,MUIA_String_Contents,&Cfg_Startup);
            if (strcmp(head[I].BT,"MMB")==0)
                set(Middle_Startup,MUIA_String_Contents,&Cfg_Startup);
            if (strcmp(head[I].BT,"RMB")==0)
                set(Right_Startup,MUIA_String_Contents,&Cfg_Startup);

            memset(Cfg_Startup,0,sizeof(Cfg_Startup)); // clear the buffer
            I++;
            fread(&head[I],sizeof(prefs),1,Cfg);
        }
        fclose(Cfg);
    }

   // open the only one window //
   set(window,MUIA_Window_Open,TRUE);

   // main loop: wait for user action //
   while (Running)
   {
        switch (DoMethod(app,MUIM_Application_Input,&sig))
        {   case MUIV_Application_ReturnID_Quit:
                Running=FALSE;
                break;

            case ID_ABOUT:
                MUI_Request(app, window, 0, NULL, "OK" ,"\33cOneTouchMac Prefs Program\n�2000 Tiphaine Julien\n\nWritten for easy configuration\n of OneTouchMac parameters");
                break;

            case BUTTON_OK:
                get(Left_Startup,MUIA_String_Contents,&LStartup);
                get(Middle_Startup,MUIA_String_Contents,&MStartup);
                get(Right_Startup,MUIA_String_Contents,&RStartup);
                if (strcmp(LStartup,"")==0 && strcmp(MStartup,"")==0 && strcmp(RStartup,"")==0)
                {   MUI_Request(app,window,0,NULL,"Ooops !","\33cYou must choose one or\nmore startup-sequence file(s)");
                    break;
                }
                if (!(Save(LStartup,MStartup,RStartup)))
                {   MUI_Request(app,window,0,NULL,"OK","\33cCould not use file\nS:Startup-sequence");
                    break;
                }
                else MUI_Request(app, window,0,NULL, "Cool !","\33cYour startup-sequence\nhas been modified\nsuccessfully");
                break;

            case BUTTON_REM:
                if (!rem())
                {   MUI_Request(app,window,0,NULL,"OK","\33cError !!!\nnothing to remove...");
                    break;
                }
                else
                {
                    MUI_Request(app, window,0,NULL, "Cool !","\33cOneTouchMac modifications\nhave been removed from\nyour startup-sequence\nsuccessfully");
                    set(Left_Startup,MUIA_String_Contents,NULL);
                    set(Middle_Startup,MUIA_String_Contents,NULL);
                    set(Right_Startup,MUIA_String_Contents,NULL);
                }
                break;

            case BUTTON_Cancel:
                Running=FALSE;
                break;
        }

        if (Running && sig)
            Wait(sig);
   }

   fail(app,NULL); // Close the application
   
}

BOOL Save(char * a,char * b, char * c)
{ return 1;} //Need reimplementation, missing from sources ASCP
BOOL rem()
{ return 1;} //Need reimplementation, missing from sources ASCP
