/*
* This file is part of OneTouchMac.
* Copyright (C) 2000 Julien Tiphaine
* 
* OneTouchMac is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* OneTouchMac is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OneTouchMac.  If not, see <http://www.gnu.org/licenses/>.
*
*/
// program:OneTouchMac V2.5
// Author:TIPHAINE Julien
// Last updated the: 23/04/2000
// Description: Alow to execute a diferent startup-sequence, given in argument,
//              by pressing LMB RMB or MMB.This prog ONLY WORK WITH AMIGA MOUSE PORT


#include <stdio.h>
#include <string.h>
#include <exec/types.h>
#include <hardware/custom.h>
#include <hardware/cia.h>

#define LEFT_BUTTON   1
#define MIDDLE_BUTTON 2
#define RIGHT_BUTTON  4

#define PORT1 1

const char *Version="$VER: OneTouchMac Version 2.6";

// link to ROM structures: always defined by the System //
extern struct Custom custom;
extern struct CIA ciaa;

int main(int argc, char **argv)
{
  // Vars //
  UBYTE Mouse(UBYTE port);
  char Commande[207]= "execute ";
  int I,Val=0;
  int LMBRang=0,MMBRang=0,RMBRang=0;

  /* Check if any arguments have been typed */
  if (argc < 2)
   {   printf("Syntaxe is: OneTouchMac <LMB|MMB|RMB> <Startup File 1> [<LMB|MMB|RMB>] [<Startup File 2>] [<LMB|MMB|RMB>] [<Startup File 3>] ");
       printf("\nTry Again ! :)\n");
       exit(0);
   }
 
  /* Check For LMB/RMB/MMB in argv in order to know witch startup have to  */
  /* be assigned to which mouse button                                     */
  for (I=1;I<argc;I++)
  { if (strcmp (argv[I],"LMB")==0 && argv[I+1]!=NULL && LMBRang==0)
        LMBRang=I+1;
    if (strcmp (argv[I],"MMB")==0 && argv[I+1]!=NULL && MMBRang==0)
        MMBRang=I+1;
    if (strcmp (argv[I],"RMB")==0 && argv[I+1]!=NULL && RMBRang==0)
        RMBRang=I+1;
  }
  
  /* Check if arguments have been typed correctly */
  if (LMBRang==0 && RMBRang==0 && MMBRang==0)
  {    printf("No startup file found ! Check that you have type LMB, RMB or MMB in upper case and that you've entered a file, or use OnetouchMac Pref to setup correctly.\n");
       exit(0);
  }

  /* get the Mouse button which is pressed */
  switch(Mouse (PORT1)) 
  {     case LEFT_BUTTON:   if (LMBRang !=0)
                            {   strcat(Commande, argv[LMBRang]);
                                system(Commande);
                            }
                            Val=20;
                            break;

        case RIGHT_BUTTON:  if (RMBRang !=0)
                            {    strcat(Commande, argv[RMBRang]);
                                 system(Commande);
                            }
                            Val=20;
                            break;

        case MIDDLE_BUTTON: if (MMBRang !=0)
                            {    strcat(Commande, argv[MMBRang]);
                                 system(Commande);
                            }
                            Val=20;
                            break;
  }

  return Val;
}


/***************** Mouse Function ********************/
/* Return values 1,2 or 4 if left, Middle or Right   */
/* Mouse button have been pressed.                   */
/* It's not a clear code, but as I need Mousse       */
/* message while no windows are opened I don't know  */
/* how to do by another way.                         */
/* This function come from Amiga C encyclopedia      */
/*****************************************************/

UBYTE Mouse(UBYTE port)
{ 
  UBYTE data = 0;
  UWORD joy, pot;

  custom.potgo = 0xFF00;
  pot = custom.potinp;

  joy = custom.joy0dat;
  data += !( ciaa.ciapra & 0x0040 ) ? LEFT_BUTTON : 0;
  data += !( pot & 0x0100 ) ? MIDDLE_BUTTON : 0;
  data += !( pot & 0x0400 ) ? RIGHT_BUTTON : 0;

return( data );

}
