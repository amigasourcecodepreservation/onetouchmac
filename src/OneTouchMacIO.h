/*
* This file is part of OneTouchMac.
* Copyright (C) 2000 Julien Tiphaine
* 
* OneTouchMac is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* OneTouchMac is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OneTouchMac.  If not, see <http://www.gnu.org/licenses/>.
*
*/
#ifndef ONETOUCHMACIO_H
#define ONETOUCHMACIO_H


// Standart C include //
#include <stdio.h>
#include <string.h>

// Amiga Include //
#include <dos/dos.h>
#include <exec/exec.h>

// protos include //
#include <proto/exec.h>
#include <clib/dos_protos.h>
#include <pragmas/dos_pragmas.h>


/************** Struct of config file *************/
typedef struct prefs { int Taille;
               char BT[4];
             } prefs;


BOOL CopyFile (char *,char *);
BOOL Save(char *,char *, char *); //Need reimplementation, missing from sources ASCP
BOOL rem(); //Need reimplementation, missing from sources ASCP

#endif
