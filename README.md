# OneTouchMac

**Description**:  Multi startup-sequence boot using mouse buttons. It's a simple program that allow you to boot from different Startup-sequence
files by Pressing the left, the right or the middle mouse button (a different file will be executed depending on witch button you've pressed).
It's totaly tranparent, unlike thoses softs that display a menu or something else. it means that your Amiga always boot as fast as possible: if no mouse
button is pressed, the original startup-sequence file is launghed, and if you press a mouse button, a different startup-sequence is executed (instead of the original one) automagicaly without any other click !

  - **Technology stack**: C, MUI 3.8
  - **Status**:  [CHANGELOG](https://gitlab.com/amigasourcecodepreservation/onetouchmac/blob/master/CHANGELOG.md).

**Screenshot**:

![TO-DO](TO-DO)


## Dependencies

* Amiga OS Classic
* MUI 3.8
* TO-DO...

## Installation

Build with SAS/C. smake in the root folder. 


## Configuration

TO-DO

## Usage

Examples:
> I use it to boot MacOS via Shapeshifter in " quick mode " without loading the WB or booting with "no startup sequence". By this way, the MacOS boot like i I had a real Mac :) I use it also to boot linux PPC directly wothout starting the workbench, thus it look like a real OS that boot without the help of another OS !
> But don't be afraid, you can boot other things than Shapeshifter or Linux PPC, it's just an exemple ! You can do for example, different startup file to boot on other hardrive, or to boot a light version of your startup-sequence to gain memory for games or something else..

## How to test the software

TO-DO

## Known issues

TO-DO

## Getting help

If you have questions, concerns, bug reports, etc, please file an issue in this repository's Issue Tracker.

## Getting involved

Contact your old amiga friends and tell them about our project, and ask them to dig out their source code or floppies and send them to us for preservation.

Clean up our hosted archives, and make the source code buildable with standard compilers like devpac, asmone, gcc 2.9x/Beppo 6.x, sas/c ,vbcc and friends.


Cheers!

Twitter
https://twitter.com/AmigaSourcePres

Gitlab

WWW
https://amigasourcepres.gitlab.io/

     _____ ___   _   __  __     _   __  __ ___ ___   _   
    |_   _| __| /_\ |  \/  |   /_\ |  \/  |_ _/ __| /_\  
      | | | _| / _ \| |\/| |  / _ \| |\/| || | (_ |/ _ \ 
     _|_| |___/_/ \_\_|_ |_|_/_/_\_\_|__|_|___\___/_/_\_\
    / __|/ _ \| | | | _ \/ __| __|  / __/ _ \|   \| __|  
    \__ \ (_) | |_| |   / (__| _|  | (_| (_) | |) | _|   
    |___/\___/_\___/|_|_\\___|___|__\___\___/|___/|___|_ 
    | _ \ _ \ __/ __| __| _ \ \ / /_\_   _|_ _/ _ \| \| |
    |  _/   / _|\__ \ _||   /\ V / _ \| |  | | (_) | .` |
    |_| |_|_\___|___/___|_|_\ \_/_/ \_\_| |___\___/|_|\_|

----

## Open source licensing info

OneTouchMac is distributed under the terms of the GNU General Public License, version 2 or later. See the [LICENSE](https://gitlab.com/amigasourcecodepreservation/onetouchmac/LICENSE.md) file for details.

----

## Credits and references

Many thanks to Julien Tiphaine for releasing the source code under GPL.

